﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPath : MonoBehaviour {
	public enum FollowType{
		v_moveTowards,
		v_lerp
	}
	public FollowType m_type = FollowType.v_moveTowards;
	public PathDefinition m_path;
	public float m_speed = 1;
	public float m_maxDistanceToGoal = 0.1f;
	private IEnumerator<Transform> m_currentPoint;
	public void Start(){
		if (m_path == null) {
			Debug.Log ("Path cannot be null", gameObject);
			return;
		}
		m_currentPoint = m_path.GetPathEnumerator ();
		m_currentPoint.MoveNext ();
		if (m_currentPoint.Current == null) {
			return;
		}
		transform.position = m_currentPoint.Current.position;
	}
	public void Update(){
		if (m_currentPoint == null || m_currentPoint == null) {
			return;
		}
		if (m_type == FollowType.v_moveTowards) {
			transform.position = Vector3.MoveTowards (transform.position, 
				m_currentPoint.Current.position, Time.deltaTime * m_speed);
		} else if (m_type == FollowType.v_lerp) {
			transform.position = Vector3.Lerp (transform.position,
				m_currentPoint.Current.position, Time.deltaTime * m_speed);
		}
		var v_distanceSquare = (transform.position
			- m_currentPoint.Current.position).sqrMagnitude;
		if (v_distanceSquare < m_maxDistanceToGoal * m_maxDistanceToGoal) {
			m_currentPoint.MoveNext ();
		}
	}
}
