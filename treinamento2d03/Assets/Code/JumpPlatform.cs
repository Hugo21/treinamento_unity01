﻿using UnityEngine;
using System.Collections;

public class JumpPlatform : MonoBehaviour {
	public float m_jumpMagnitude = 20;

	public void ControllerEnter2D(CharacterController2D v_controller){
		v_controller.SetVerticalForce (m_jumpMagnitude);
	}
}
