﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	private bool m_isFacingRight;
	private CharacterController2D m_controller;
	private float m_normalizedHorizontalSpeed;
	public float m_maxSpeed = 8f;
	public float m_speedAccelerationOnGround = 10f;
	public float m_speedAccererationInAir = 5f;

	public void Start(){
		m_controller = GetComponent<CharacterController2D> ();
		m_isFacingRight = transform.localScale.x > 0;
	}

	public void Update(){
		HandleInput ();
		var v_movementFactor = m_controller.State.IsGrounded 
			? m_speedAccelerationOnGround : m_speedAccererationInAir;
		m_controller.SetHorizontalForce (Mathf.Lerp (m_controller.Velocity.x,
			m_normalizedHorizontalSpeed*m_maxSpeed, Time.deltaTime*v_movementFactor));
	}

	private void HandleInput(){
		if (Input.GetKey (KeyCode.D)) {
			m_normalizedHorizontalSpeed = 1;
			if (!m_isFacingRight) {
				Flip ();
			}
		} else if (Input.GetKey (KeyCode.A)) {
			m_normalizedHorizontalSpeed = -1;
			if (m_isFacingRight) {
				Flip ();
			}
		} else {
			m_normalizedHorizontalSpeed = 0;
		}
		if (m_controller.CanJump && Input.GetKey (KeyCode.Space)) {
			m_controller.Jump ();
		}
	}

	private void Flip(){
		transform.localScale = new Vector3 (-transform.localScale.x,
			transform.localScale.y, transform.localScale.z);
		m_isFacingRight = transform.localScale.x > 0;
	}
}
