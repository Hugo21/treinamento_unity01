﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Linq;
using System;

public class PathDefinition : MonoBehaviour {
	public Transform[] m_points;
	public IEnumerator<Transform> GetPathEnumerator(){
		if (m_points == null || m_points.Length < 1) {
			yield break;
		}
		var v_direction = 1;
		var v_index = 0;
		while (true) {
			yield return m_points [v_index];
			if (m_points.Length == 1) {
				continue;
			}
			if (v_index <= 0) {
				v_direction = 1;
			} else if (v_index >= m_points.Length - 1) {
				v_direction = -1;
			}
			v_index = v_index + v_direction;
		}
	}

	public void OnDrawGizmos(){
		if (m_points == null || m_points.Length < 2) {
			return;
		}
		var v_points = m_points.Where (t => t != null).ToList ();
		if (v_points.Count < 2) {
			return;
		}
		for (var v_i = 1; v_i < v_points.Count; v_i++) {
			Gizmos.DrawLine (v_points[v_i-1].position, v_points[v_i].position);
		}
	}
}
