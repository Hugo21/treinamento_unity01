﻿using UnityEngine;
using System.Collections;

public class CharacterController2D : MonoBehaviour {
	public const float m_skinWidth = 0.02f;
	public const int m_totalHorizontal = 8;
	public const int m_totalVerticalRays = 4;
	private static readonly float m_slopeLimitTangent = Mathf.Tan (75f*Mathf.Deg2Rad);
	public LayerMask m_platformLayerMask;
	public ControllerParameters2D m_defaultParameters;
	public ControllerState2D State{ get; private set;}
	public Vector2 Velocity{ get{return m_velocity;}}
	public bool HandleCollisions{ get; set;}
	public ControllerParameters2D Parameters{get{return m_overrideParameters ?? m_defaultParameters;}}
	public GameObject StandingOn{ get; private set;}
	public Vector3 PlatformVelocity{ get; private set;}
	public bool CanJump{
		get{
			if (Parameters.m_jumpBehaviorRestrictions == ControllerParameters2D.JumpBehavior.v_canJumpAnywhere) {
				return m_jumpIn <= 0;
			}
			if (Parameters.m_jumpBehaviorRestrictions == ControllerParameters2D.JumpBehavior.v_canJumpOnGround) {
				return State.IsGrounded;
			}
			return false;
		}
	}

	private Vector2 m_velocity;
	private Transform m_transform;
	private Vector3 m_localScale;
	private BoxCollider2D m_boxCollider2D;
	private ControllerParameters2D m_overrideParameters;
	private float m_jumpIn;
	private GameObject m_lastStandingOn;

	private Vector3
		m_activeGlobalPlatformPoint,
		m_activeLocalPlatformPoint;

	private Vector3
		m_rayCastTopLeft,
		m_rayCastBottomRight,
		m_rayCastBottomLeft;

	private float 
		m_verticalDistanceBetweenRays,
		m_horizontalDistanceBetweenRays;

	public void Awake(){
		HandleCollisions = true;
		State = new ControllerState2D ();
		m_transform = transform;
		m_localScale = transform.localScale;
		m_boxCollider2D = GetComponent<BoxCollider2D> ();
		var v_colliderWidth = m_boxCollider2D.size.x *
		                      Mathf.Abs (transform.localScale.x) - (2 * m_skinWidth);
		m_horizontalDistanceBetweenRays = v_colliderWidth / (m_totalVerticalRays - 1);
		var v_colliderHeight = m_boxCollider2D.size.y *
		                       Mathf.Abs (transform.localScale.y) - (2 * m_skinWidth);
		m_verticalDistanceBetweenRays = v_colliderHeight / (m_totalHorizontal - 1);
	}

	public void AddForce(Vector2 v_force){
		m_velocity += v_force;
	}

	public void SetForce(Vector2 v_force){
		m_velocity = v_force;
	}

	public void SetHorizontalForce(float v_x){
		m_velocity.x = v_x;
	}

	public void SetVerticalForce(float v_y){
		m_velocity.y = v_y;
	}

	public void Jump(){
		AddForce (new Vector2(0, Parameters.m_jumpMagnitude));
		m_jumpIn = Parameters.m_jumpFrequence;
	}

	public void LateUpdate(){
		m_jumpIn -= Time.deltaTime;
		m_velocity.y += Parameters.m_gravity * Time.deltaTime;
		Move (Velocity * Time.deltaTime);
	}

	private void Move(Vector2 v_deltaMovement){
		var v_wasGrounded = State.IsCollidingBelow;
		State.Reset ();
		if (HandleCollisions) {
			HandlePlatforms ();
			CalculateRayOrigins ();
			if (v_deltaMovement.y < 0 && v_wasGrounded) {
				HandleVerticalSlope (ref v_deltaMovement);
			}
			if (Mathf.Abs (v_deltaMovement.x) > 0.001f) {
				MoveHorizontally (ref v_deltaMovement);
			}
			MoveVertically (ref v_deltaMovement);
		}
		m_transform.Translate (v_deltaMovement, Space.World);
		if (Time.deltaTime > 0) {
			m_velocity = v_deltaMovement / Time.deltaTime;
		}
		m_velocity.x = Mathf.Min (m_velocity.x, Parameters.m_maxVelocity.x);
		m_velocity.y = Mathf.Min (m_velocity.y, Parameters.m_maxVelocity.y);
		if (State.IsMovingUpSlope) {
			m_velocity.y = 0;
		}
		if (StandingOn != null) {
			m_activeGlobalPlatformPoint = transform.position;
			m_activeLocalPlatformPoint = StandingOn.transform.InverseTransformPoint (transform.position);

			if (m_lastStandingOn != StandingOn) {
				if (m_lastStandingOn != null) {
					m_lastStandingOn.SendMessage ("ControllerExit2d", this, SendMessageOptions.DontRequireReceiver);
				}
				StandingOn.SendMessage ("ControllerEnter2D", this, SendMessageOptions.DontRequireReceiver);
				m_lastStandingOn = StandingOn;
			} else if (StandingOn != null) {
				StandingOn.SendMessage ("ControllerStay2D", this, SendMessageOptions.DontRequireReceiver);
			}
		} else if (m_lastStandingOn != null) {
			m_lastStandingOn.SendMessage ("ControllerExit2D", this, SendMessageOptions.DontRequireReceiver);
			m_lastStandingOn = null;
		}
	}

	private void HandlePlatforms(){
		if (StandingOn != null) {
			var v_newGlobalPlatformPoint = StandingOn.transform.TransformPoint (m_activeLocalPlatformPoint);
			var v_moveDistance = v_newGlobalPlatformPoint - m_activeGlobalPlatformPoint;
			if (v_moveDistance != Vector3.zero) {
				transform.Translate (v_moveDistance, Space.World);
			}
			PlatformVelocity = (v_newGlobalPlatformPoint - m_activeGlobalPlatformPoint) / Time.deltaTime;
		} else {
			PlatformVelocity = Vector3.zero;
		}
		StandingOn = null;
	}

	private void CalculateRayOrigins(){
		var v_size = new Vector2 (m_boxCollider2D.size.x * Mathf.Abs (m_localScale.x),
			m_boxCollider2D.size.y * Mathf.Abs (m_localScale.y))/2;
		var v_center = new Vector2 (m_boxCollider2D.offset.x * m_localScale.x,
			               m_boxCollider2D.offset.y * m_localScale.y);
		m_rayCastTopLeft = m_transform.position +
		new Vector3 (v_center.x - v_size.x + m_skinWidth,
			v_center.y + v_size.y - m_skinWidth);
		m_rayCastBottomRight = m_transform.position +
		new Vector3 (v_center.x + v_size.x - m_skinWidth,
			v_center.y - v_size.y + m_skinWidth);
		m_rayCastBottomLeft = m_transform.position +
			new Vector3 (v_center.x - v_size.x + m_skinWidth,
				v_center.y - v_size.y + m_skinWidth);
	}

	private void MoveHorizontally(ref Vector2 v_deltaMovement){
		var v_isGoingRight = v_deltaMovement.x > 0;
		var v_rayDistance = Mathf.Abs (v_deltaMovement.x) + m_skinWidth;
		var v_rayDirection = v_isGoingRight ? Vector2.right : -Vector2.right;
		var v_rayOrigin = v_isGoingRight ? m_rayCastBottomRight : m_rayCastBottomLeft;
		for (var i = 0; i < m_totalVerticalRays; i++){
			var v_rayVector = new Vector2 (v_rayOrigin.x,
				v_rayOrigin.y + (i * m_verticalDistanceBetweenRays));
			Debug.DrawRay (v_rayVector, v_rayDirection * v_rayDistance, Color.red);
			var v_rayCastHit = Physics2D.Raycast (v_rayVector, v_rayDirection, v_rayDistance, m_platformLayerMask);
			if (!v_rayCastHit) {
				continue;
			}
			if (i == 0 && HandleHorizontalSlope (ref v_deltaMovement, Vector2.Angle (v_rayCastHit.normal, Vector2.up), v_isGoingRight)) {
				break;
			}
			v_deltaMovement.x = v_rayCastHit.point.x - v_rayVector.x;
			v_rayDistance = Mathf.Abs (v_deltaMovement.x);
			if (v_isGoingRight) {
				v_deltaMovement.x -= m_skinWidth;
				State.IsCollidingRight = true;
			} else {
				v_deltaMovement.x += m_skinWidth;
				State.IsCollidingLeft = true;
			}
			if (v_rayDistance < m_skinWidth + 0.0001f) {
				break;
			}
		}
	}

	private void MoveVertically(ref Vector2 v_deltaMovement){
		var v_isGoingUp = v_deltaMovement.y > 0;
		var v_rayDistance = Mathf.Abs (v_deltaMovement.y) + m_skinWidth;
		var v_rayDirection = v_isGoingUp ? Vector2.up : - Vector2.up;
		var v_rayOrigin = v_isGoingUp ? m_rayCastTopLeft : m_rayCastBottomLeft;

		v_rayOrigin.x += v_deltaMovement.x;

		var v_standingOnDistance = float.MaxValue;
		for (var i = 0; i < m_totalVerticalRays; i++) {
			var v_rayVector = new Vector2 (v_rayOrigin.x + (i * m_horizontalDistanceBetweenRays), v_rayOrigin.y);
			Debug.DrawRay (v_rayVector, v_rayDirection * v_rayDistance, Color.red);
			var v_rayCastHit = Physics2D.Raycast (v_rayVector, v_rayDirection, v_rayDistance, m_platformLayerMask);
			if (!v_rayCastHit) {
				continue;
			}
			if (!v_isGoingUp) {
				var v_verticalDistanceToHit = m_transform.position.y - v_rayCastHit.point.y;
				if (v_verticalDistanceToHit < v_standingOnDistance) {
					v_standingOnDistance = v_verticalDistanceToHit;
					StandingOn = v_rayCastHit.collider.gameObject;
				}
			}
			v_deltaMovement.y = v_rayCastHit.point.y - v_rayVector.y;
			v_rayDistance = Mathf.Abs (v_deltaMovement.y);
			if (v_isGoingUp) {
				v_deltaMovement.y -= m_skinWidth;
				State.IsCollidingAbove = true;
			} else {
				v_deltaMovement.y += m_skinWidth;
				State.IsCollidingBelow = true;
			}
			if (!v_isGoingUp && v_deltaMovement.y > 0.0001f) {
				State.IsMovingUpSlope = true;
			}
			if (v_rayDistance < m_skinWidth + 0.0001f) {
				break;
			}
		}
	}

	private void HandleVerticalSlope(ref Vector2 v_deltaMovement){
		var v_center = (m_rayCastBottomLeft.x + m_rayCastBottomRight.x) / 2;
		var v_direction = -Vector2.up;
		var v_slopeDistance = m_slopeLimitTangent * (m_rayCastBottomRight.x - v_center);
		var v_slopeRayVector = new Vector2 (v_center, m_rayCastBottomLeft.y);
		Debug.DrawRay (v_slopeRayVector, v_direction * v_slopeDistance, Color.yellow);
		var v_rayCastHit = Physics2D.Raycast (v_slopeRayVector, v_direction, v_slopeDistance, m_platformLayerMask);
		if (!v_rayCastHit) {
			return;
		}

		// Resharper disable CompareOfFloatsByEqualityOperator

		var v_isMovingDownSlope = Mathf.Sign (v_rayCastHit.normal.x) == Mathf.Sign (v_deltaMovement.x);
		if (!v_isMovingDownSlope) {
			return;
		}

		var v_angle = Vector2.Angle (v_rayCastHit.normal, Vector2.up);
		if (Mathf.Abs (v_angle) < 0.0001f) {
			return;
		}
		State.IsMovingDownSlope = true;
		State.SlopeAngle = v_angle;
		v_deltaMovement.y = v_rayCastHit.point.y - v_slopeRayVector.y;
	}	

	private bool HandleHorizontalSlope(ref Vector2 v_deltaMovement, float v_angle,
		bool v_isGoingRight){
		if (Mathf.RoundToInt (v_angle) == 90) {
			return false;
		}
		if (v_angle > Parameters.m_slopeLimit) {
			v_deltaMovement.x = 0;
			return true;
		}
		if (v_deltaMovement.y > 0.07f) {
			return true;
		}
		v_deltaMovement.x += v_isGoingRight ? -m_skinWidth : m_skinWidth;
		v_deltaMovement.y = Mathf.Abs (Mathf.Tan (v_angle * Mathf.Deg2Rad) * v_deltaMovement.x);
		State.IsMovingUpSlope = true;
		State.IsCollidingBelow = true;
		return true;
	}

	public void OnTriggerEnter2D(Collider2D v_other){

	}

	public void OnTriggerExit2D(Collider2D v_other){

	}
}
