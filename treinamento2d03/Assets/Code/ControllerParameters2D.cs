﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

[System.Serializable]
public class ControllerParameters2D : System.Object {
	public enum JumpBehavior{
		v_canJumpOnGround,
		v_canJumpAnywhere,
		v_cantJump
	}
	public Vector2 m_maxVelocity = new Vector2(float.MaxValue, float.MaxValue);
	[Range(0f, 90f)]
	public float m_slopeLimit = 30f;
	public float m_gravity = -25f;
	public JumpBehavior m_jumpBehaviorRestrictions;
	public float m_jumpFrequence = 0.25f;
	public float m_jumpMagnitude = 12f;
}
