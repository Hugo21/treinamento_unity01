﻿using UnityEngine;
using System.Collections;

public class SpikySkill : MonoBehaviour {
	public float m_speed;
	private Animator m_animator;
	private float m_currentDestroyTime;
	public float m_destroyTime;
	private PlayerBehavior m_player;
	private BoxCollider2D m_boxCollider2D;

	// Use this for initialization
	void Start () {
		m_animator = GetComponent<Animator> ();
		m_player = FindObjectOfType (typeof(PlayerBehavior)) as PlayerBehavior;
		m_boxCollider2D = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		m_currentDestroyTime += Time.deltaTime;
		if (m_currentDestroyTime >= m_destroyTime) {
			Destroy (gameObject);
		}
		Movement ();
	}

	private void Movement(){
		transform.Translate (new Vector3(m_speed*Time.deltaTime, 0, 0));
	}

	void OnTriggerEnter2D(Collider2D v_other){
		if (v_other.gameObject.tag == "Player") {
			m_speed = 0f;
			m_animator.SetTrigger ("Hit");
			m_boxCollider2D.enabled = false;
			m_player.m_lifes -= 1;
		}
	}

	public void DestroySkill(){
		//yield return new WaitForSeconds (0f);
		Destroy (gameObject);
	}
}
