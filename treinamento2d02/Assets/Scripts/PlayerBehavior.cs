﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : MonoBehaviour {
	public float m_speed;
	private Animator m_animator;
	private Rigidbody2D m_rigidBody2d;
	public Transform m_ground;
	private bool m_isGround;
	private GameController m_gameController;
	public GameObject m_playerHand;
	public float m_launchSkillTime;
	private float m_currentLaunchSkillTime;
	public Transform m_prefabPlayerSkill;
	public int m_lifes;
	private float m_inputH;

	// Use this for initialization
	void Start () {
		m_animator = GetComponent<Animator> ();
		m_rigidBody2d = GetComponent<Rigidbody2D> ();
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
		m_currentLaunchSkillTime = m_launchSkillTime;
		m_lifes = 3;
	}
	
	// Update is called once per frame
	void Update () {
		if (m_gameController.CurrentState == StateMachine.IN_GAME) {
			#if !UNITY_ANDROID || !UNITY_IPHONE
			Moviment (Input.GetAxis ("Horizontal"));
			#else
			Moviment (m_inputH);
			#endif
			Jump (0);
			SkillLaunch (0);
		}
	}

	public void Moviment (float v_inputHorizontal){
		m_isGround = Physics2D.Linecast (transform.position, m_ground.position, 
			1 << LayerMask.NameToLayer ("Ground"));
		if (v_inputHorizontal < 0) {
			transform.Translate (Vector2.right * m_speed * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 180);
			m_animator.SetFloat ("Run", Mathf.Abs (v_inputHorizontal));
		} else if (v_inputHorizontal > 0) {
			transform.Translate (Vector2.right * m_speed * Time.deltaTime);
			transform.eulerAngles = new Vector2 (0, 0);
			m_animator.SetFloat ("Run", Mathf.Abs (v_inputHorizontal));
		} else if (v_inputHorizontal == 0) {
			m_inputH = 0;
			m_animator.SetFloat ("Run", 0);
		}
	}

	public void NormalizeInputH(float v_inputHNormalized){
		m_inputH = v_inputHNormalized;
	}

	public void Jump(float v_jump){
		#if !UNITY_ANDROID || !UNITY_IPHONE
		if (m_isGround && Input.GetKey (KeyCode.Space)) {
			m_animator.SetBool ("Jump", !m_isGround);
			m_rigidBody2d.AddForce (Vector2.up * 300);
		} else {
			m_animator.SetBool ("Jump", !m_isGround);
		}
		#else
		if (v_jump == 1) {
			m_animator.SetBool ("Jump", !m_isGround);
			m_rigidBody2d.AddForce (Vector2.up * 300);
		} else {
			m_animator.SetBool ("Jump", !m_isGround);
		}
		#endif
	}

	public void SkillLaunch(float v_skill){
		m_currentLaunchSkillTime += Time.deltaTime;
		#if !UNITY_ANDROID || !UNITY_IPHONE
		if(Input.GetKeyDown (KeyCode.F) ){
			if (m_currentLaunchSkillTime > m_launchSkillTime) {
				Instantiate (m_prefabPlayerSkill, 
					m_playerHand.transform.position, transform.rotation);
				m_currentLaunchSkillTime = 0;
			}
		}
		#else
		if(v_skill == 1){
			if (m_currentLaunchSkillTime > m_launchSkillTime) {
				Instantiate (m_prefabPlayerSkill, 
					m_playerHand.transform.position, transform.rotation);
				m_currentLaunchSkillTime = 0;
			}
		}
		#endif
	}
}
