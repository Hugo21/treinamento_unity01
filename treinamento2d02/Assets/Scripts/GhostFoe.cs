﻿using UnityEngine;
using System.Collections;

public class GhostFoe : BaseFoe {
	public float m_timeCheck;
	private float m_currentTimeCheck;
	private bool m_rightMovement;
	public Transform m_distanceInit;
	public Transform m_distanceEnd;
	public float m_timeToDrop;
	private float m_currentTimeToDrop;
	private bool m_isPlayerOnArea;

	// Use this for initialization
	void Start () {
		base.Start ();
		m_speed *= -1;
	}
	
	// Update is called once per frame
	void Update () {
		if (m_gameController.CurrentState == StateMachine.IN_GAME) {
			Movement ();
			RayCast ();
		}
	}

	private void Movement(){
		m_currentTimeCheck += Time.deltaTime;
		m_currentTimeToDrop += Time.deltaTime;
		if (m_currentTimeCheck >= m_timeCheck) {
			//m_speed *= -1;
			if (!m_rightMovement) {
				transform.eulerAngles = new Vector2 (0, 180);
				m_rightMovement = true;
			} else {
				transform.eulerAngles = new Vector2 (0, 0);
				m_rightMovement = false;
			}
			m_currentTimeCheck = 0;
		}
		transform.Translate (m_speed*Time.deltaTime, 0, 0);
	}

	private void RayCast(){
		Debug.DrawLine(m_distanceInit.position, m_distanceEnd.position, Color.red);
		m_isPlayerOnArea = Physics2D.Linecast (m_distanceInit.position, m_distanceEnd.position,
			1 << LayerMask.NameToLayer ("Player"));
		if (m_isPlayerOnArea && m_currentTimeToDrop > m_timeToDrop) {
			//Debug.Log ("GGGGGGGGGGG");
			Instantiate (m_prefabSkill, transform.position, transform.rotation);
			m_currentTimeToDrop = 0;
		}
	}
}
