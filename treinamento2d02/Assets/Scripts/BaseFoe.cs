﻿using UnityEngine;
using System.Collections;

public class BaseFoe : MonoBehaviour {
	public float m_speed;
	public GameObject m_prefabSkill;
	protected GameController m_gameController;

	// Use this for initialization
	protected void Start () {
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
