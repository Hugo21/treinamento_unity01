﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {
	private GameController m_gameController;

	// Use this for initialization
	void Start () {
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D v_other){
		if (v_other.gameObject.tag == "Player") {
			m_gameController.m_coinCount += 1;
			m_gameController.m_coinCountText.text = m_gameController.m_coinCount.ToString ();
			Destroy (gameObject);
		}
	}
}
