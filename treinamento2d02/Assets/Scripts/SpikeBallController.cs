﻿using UnityEngine;
using System.Collections;

public class SpikeBallController : MonoBehaviour {
	private Animator m_animator;
	private PlayerBehavior m_player;

	// Use this for initialization
	void Start () {
		m_animator = GetComponent<Animator> ();
		m_player = FindObjectOfType (typeof(PlayerBehavior)) as PlayerBehavior;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D v_other){
		m_animator.SetTrigger ("TTTTTTTTTTT");
		if (v_other.gameObject.tag == "Player") {
			m_player.m_lifes -= 1;
		}
	}

	IEnumerator DestroyGameObject(){
		yield return new WaitForSeconds (1);
		Destroy (gameObject);
	}
}
