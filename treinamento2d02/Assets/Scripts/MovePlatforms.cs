﻿using UnityEngine;
using System.Collections;
using System;

public class MovePlatforms : MonoBehaviour {
	public float m_speed;
	private GameController m_gameController;

	// Use this for initialization
	void Start () {
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
	}
	
	// Update is called once per frame
	void Update () {
		if(m_gameController.CurrentState == StateMachine.IN_GAME){
			Movement ();
		}
	}
		
	void Movement(){
		transform.Translate (0, m_speed * Time.deltaTime, 0);
	}

	void OnTriggerEnter2D(Collider2D v_other){
		if (v_other.gameObject.tag == "LimitPlatformsMovement") {
			m_speed *= -1;
		}
	}
}
