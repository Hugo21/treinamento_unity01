﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseController : MonoBehaviour {
	public GameObject m_pausePanel;
	private bool m_isPaused;
	private GameController m_gameController;

	// Use this for initialization
	void Start () {
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Paused(){
		if (!m_isPaused) {
			m_gameController.CurrentState = StateMachine.PAUSED;
			m_pausePanel.SetActive (true);
		} else {
			m_gameController.CurrentState = StateMachine.IN_GAME;
			m_pausePanel.SetActive (false);
		}
		m_isPaused = !m_isPaused;
	}
}
