﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {
	private Vector2 m_speed;
	private float m_smooth = 0.5f;
	private GameObject m_player;

	// Use this for initialization
	void Start () {
		m_player = GameObject.Find ("Player");
		m_speed = new Vector2 (0.5f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		Follow ();
	}

	private void Follow(){
		Vector2 v_positionCamera = Vector2.zero;
		v_positionCamera.x = Mathf.SmoothDamp (transform.position.x,
			m_player.transform.position.x, ref m_speed.x, m_smooth);
		v_positionCamera.y = Mathf.SmoothDamp (transform.position.y,
			m_player.transform.position.y, ref m_speed.y, m_smooth);
		Vector3 v_newPosition = new Vector3 (v_positionCamera.x, v_positionCamera.y,
			                        transform.position.z);
		transform.position = Vector3.Lerp (transform.position, v_newPosition, Time.time);
	}
}
