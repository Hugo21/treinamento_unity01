﻿using UnityEngine;
using System.Collections;
using UnityEngineInternal;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public enum StateMachine{
	START_GAME,
	IN_GAME,
	PAUSED,
	GAME_OVER,
	WIN
}

public class GameController : MonoBehaviour {
	private StateMachine m_stateMachine;
	public Sprite[] m_lifeArraySprite;
	private PlayerBehavior m_player;
	public Image m_lifesImg;
	public int m_coinCount;
	public Text m_coinCountText;
	private Vector3 m_playerInitialPosition;
	public GameObject m_successPanel;
	public GameObject m_mobileController;

	// Use this for initialization
	void Start () {
		#if !UNITY_ANDROID || !UNITY_IPHONE
		m_mobileController.SetActive (false);
		#else
		#endif
		m_stateMachine = StateMachine.START_GAME;
		m_player = FindObjectOfType (typeof(PlayerBehavior)) as PlayerBehavior;
	}
	
	// Update is called once per frame
	void Update () {
		CurrentStateMachine ();
	}

	private void CurrentStateMachine(){
		switch (m_stateMachine) {
		case StateMachine.START_GAME:
			{
				InGame ();
			}
			break;
		case StateMachine.IN_GAME:
			{
				CheckPlayerLifes ();
			}
			break;
		case StateMachine.PAUSED:
			{
			}
			break;
		case StateMachine.WIN:
			{
			}
			break;
		case StateMachine.GAME_OVER:
			{
			}
			break;
		}
	}

	//Função set in get
	public StateMachine CurrentState{
		get{ return m_stateMachine; }
		set{ m_stateMachine = value; }
	}

	public void InGame(){
		m_playerInitialPosition = m_player.transform.position;
		CurrentState = StateMachine.IN_GAME;
	}

	public void CheckPlayerLifes(){
		if (m_player.m_lifes == 1) {
			m_lifesImg.sprite = m_lifeArraySprite[0];
		} else if (m_player.m_lifes == 2) {
			m_lifesImg.sprite = m_lifeArraySprite[1];
		} else if (m_player.m_lifes == 3) {
			m_lifesImg.sprite = m_lifeArraySprite[2];
		} else {
			SceneManager.LoadScene ("GameOver");
			Debug.Log ("Game Over");
		}
	}

	public void Fail(){
		m_player.transform.position = m_playerInitialPosition;
	}

	public void Success(){
		m_successPanel.SetActive (true);
	}
}
