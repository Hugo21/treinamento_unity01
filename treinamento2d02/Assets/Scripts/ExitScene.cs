﻿using UnityEngine;
using System.Collections;

public class ExitScene : MonoBehaviour {
	private GameObject m_player;
	private Vector3 m_initialPosition;

	// Use this for initialization
	void Start () {
		m_player = GameObject.FindWithTag ("Player");
		m_initialPosition = m_player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D v_other){
		if (v_other.gameObject.tag == "Player") {
			Debug.Log ("Saiu");
			m_player.GetComponent<PlayerBehavior> ().m_lifes -= 1;
			m_player.transform.position = m_initialPosition;
		}
	}
}
