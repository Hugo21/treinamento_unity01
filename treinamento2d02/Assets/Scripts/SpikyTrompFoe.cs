﻿using UnityEngine;
using System.Collections;

public class SpikyTrompFoe : BaseFoe {
	private GameObject m_player;
	private bool m_playerIsFind;
	private Rigidbody2D m_rigidBody2D;
	private bool m_flipSprite;
	public bool m_jump;
	public Transform m_i;
	public Transform m_j;
	private bool m_skill;
	private float m_currentLauchSkillTime;
	public float m_LauchSkillTime;
	private Animator m_animator;
	public GameObject m_coinPrefab;
	private CircleCollider2D m_circleCollider2D;

	// Use this for initialization
	void Start () {
		base.Start ();
		m_player = GameObject.FindWithTag ("Player");
		m_rigidBody2D = GetComponent<Rigidbody2D> ();
		m_animator = GetComponent<Animator> ();
		m_circleCollider2D = GetComponent<CircleCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (m_gameController.CurrentState == StateMachine.IN_GAME) {
			LookingAtPlayer ();
			Movement ();
			RayCasting ();
			Jump ();
			Skill ();
		}
	}

	private void LookingAtPlayer(){
		if (Vector2.Distance (m_player.transform.position, transform.position) <
		    5.0f) {
			m_playerIsFind = true;
			Debug.Log ("LLLPLPLPLPL");
			m_skill = true;
		} else {
			m_playerIsFind = false;
			m_skill = false;
		}
	}

	private void Movement(){
		if(m_playerIsFind){
			if (transform.position.x < m_player.transform.position.x) {
				m_rigidBody2D.velocity = new Vector3 (m_speed, 0, 0);
				if (m_flipSprite) {
					//transform.localScale = new Vector3 (transform.localScale.x  * (-1),
						//transform.localScale.y, transform.localScale.z);
					transform.eulerAngles = new Vector3(0, 0, 0);
					m_flipSprite = false;
				}
			} else if (transform.position.x > m_player.transform.position.x) {
				m_rigidBody2D.velocity = new Vector3 (m_speed*(-1), 0, 0);
				if (!m_flipSprite) {
					//transform.localScale = new Vector3 (transform.localScale.x,
						//transform.localScale.y, transform.localScale.z);
					transform.eulerAngles = new Vector3(0, 180, 0);
					m_flipSprite = true;
				}
			}
		}
	}

	private void RayCasting(){
		if (Physics2D.Linecast (m_i.position, m_j.position)) {
			m_jump = true;
			Debug.Log ("Pulou");
		} else {
			m_jump = false;
		}
	}

	private void Jump(){
		if (m_jump && m_skill) {
			m_rigidBody2D.AddForce (new Vector2(0, 1000));
		}
	}

	private void Skill(){
		if (m_skill) {
			m_currentLauchSkillTime += Time.deltaTime;
			if (m_currentLauchSkillTime >= m_LauchSkillTime
			&& m_circleCollider2D.enabled) {
				Instantiate (m_prefabSkill, transform.position,
					transform.rotation);
				m_currentLauchSkillTime = 0;
				m_animator.SetBool ("Skill", m_skill);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D v_other){
		if (v_other.gameObject.tag == "PlayerSkill") {
			m_circleCollider2D.enabled = false;
			m_animator.SetTrigger ("Defeat");
		}
	}

	IEnumerator Defeat(){
		Instantiate (m_coinPrefab, transform.position, transform.rotation);
		yield return new WaitForSeconds (0);
		Destroy (gameObject);
	}
}
