﻿using UnityEngine;
using System.Collections;

public class SuccessCheck : MonoBehaviour {
	private GameController m_gameController;

	// Use this for initialization
	void Start () {
		m_gameController = FindObjectOfType (typeof(GameController)) as GameController;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D v_other){
		if (v_other.gameObject.tag == "Player") {
			if (m_gameController.m_coinCount >= 5) {
				m_gameController.CurrentState = StateMachine.WIN;
				m_gameController.Success ();
			} else {
				m_gameController.Fail ();
			}
		}
	}
}
