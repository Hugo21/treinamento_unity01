﻿using UnityEngine;
using System.Collections;

public class spawnController : MonoBehaviour {
	public GameObject m_obstaclePrefab;
	public float m_spawnRate;
	private float m_currentTime;
	private int m_position;
	private float m_y;
	public float m_posA;
	public float m_posB;

	// Use this for initialization
	void Start () {
		m_currentTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		m_currentTime += Time.deltaTime;
		if (m_currentTime >= m_spawnRate) {
			m_currentTime = 0;
			m_position = Random.Range (1, 10);
			if (m_position > 5) {
				m_y = m_posA;
			} else {
				m_y = m_posB;
			}
			GameObject v_tempPrefab = Instantiate (m_obstaclePrefab) as GameObject;
			v_tempPrefab.transform.position = new Vector3 (transform.position.x,
				m_y, v_tempPrefab.transform.position.z);
		}
	}
}
