﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	public UnityEngine.UI.Text m_score;
	public UnityEngine.UI.Text m_record;

	// Use this for initialization
	void Start () {
		m_score.text = PlayerPrefs.GetInt ("Score").ToString ();
		m_record.text = PlayerPrefs.GetInt ("Record").ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
