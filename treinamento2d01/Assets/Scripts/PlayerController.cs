﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	public Rigidbody2D m_playerRigidbody2D;
	public int m_forceJump;
	public Animator m_playerAnimator;

	private bool m_jump;
	private bool m_slide;
	// Verificar o chão
	public bool m_grounded;
	public LayerMask m_whatIsGround;
	public Transform m_groundCheck;
	// Slide
	public float m_slideDuration;
	public float m_timer;
	// Colisor
	public Transform m_collider;
	// Audio
	public AudioSource m_audio;
	public AudioClip m_jumpSound;
	public AudioClip m_slideSound;
	// Pontuação
	public UnityEngine.UI.Text m_txtScore;
	public static int m_score;

	// Use this for initialization
	void Start () {
		m_score = 0;
		PlayerPrefs.SetInt ("Score", m_score);
	}
	
	// Update is called once per frame
	void Update () {
		m_txtScore.text = m_score.ToString ();
		if(Input.GetButtonDown ("Jump") && m_grounded){
			if (!m_jump) {
				Debug.Log ("Pulou");
				m_playerRigidbody2D.AddForce (new Vector2(0, m_forceJump));
				m_jump = true;
				m_audio.PlayOneShot (m_jumpSound);
			}
			if (m_slide) {
				m_collider.position = new Vector3 (m_collider.position.x, m_collider.position.y + 0.3f, m_collider.position.z);
				m_slide = false;
			}
		}
		if(Input.GetButtonDown ("Slide") && m_grounded){
			if (!m_slide) {
				Debug.Log ("Deslizou");
				m_slide = true;
				m_collider.position = new Vector3 (m_collider.position.x, m_collider.position.y - 0.3f, m_collider.position.z);
				m_timer = 0f;
				m_audio.PlayOneShot (m_slideSound);
			}
		}
		m_grounded = Physics2D.OverlapCircle (m_groundCheck.position, 0.02f, m_whatIsGround);
		m_jump = !m_grounded;
		if (m_slide) {
			m_timer += Time.deltaTime;
			if (m_timer >= m_slideDuration) {
				m_slide = false;
				m_collider.position = new Vector3 (m_collider.position.x, m_collider.position.y + 0.3f, m_collider.position.z);
			}
		}
		m_playerAnimator.SetBool ("AnimatorJump", m_jump);
		m_playerAnimator.SetBool ("AnimatorSlide", m_slide);
	}

	void OnTriggerEnter2D(){
		Debug.Log ("Bateu");
		PlayerPrefs.SetInt ("Score", m_score);
		if(m_score > PlayerPrefs.GetInt ("Record")){
			PlayerPrefs.SetInt("Record", m_score);
		}
		SceneManager.LoadScene ("GameOver");
	}
}
