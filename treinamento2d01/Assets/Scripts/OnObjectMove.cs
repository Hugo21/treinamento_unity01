﻿using UnityEngine;
using System.Collections;

public class OnObjectMove : MonoBehaviour {
	public float m_speed;
	private float m_x;
	public GameObject m_player;
	private bool m_scoreIsCounted;

	// Use this for initialization
	void Start () {
		m_player = GameObject.Find ("Player") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
		m_x = transform.position.x;
		m_x += m_speed * Time.deltaTime;
		transform.position = new Vector3 (m_x, transform.position.y, transform.position.z);
		if (m_x <= -7) {
			Destroy (transform.gameObject);
		}
		if (m_x < m_player.transform.position.x && !m_scoreIsCounted) {
			m_scoreIsCounted = true;
			PlayerController.m_score += 1;
		}
	}
}
