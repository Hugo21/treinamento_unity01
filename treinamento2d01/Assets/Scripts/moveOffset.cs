﻿using UnityEngine;
using System.Collections;

public class moveOffset : MonoBehaviour {
	private Material m_currentMaterial;
	public float m_speed;
	private float m_offSet;

	// Use this for initialization
	void Start () {
		m_currentMaterial = GetComponent<Renderer> ().material;
	}
	
	// Update is called once per frame
	void Update () {
		m_offSet += m_speed * Time.deltaTime;
		m_currentMaterial.SetTextureOffset ("_MainTex", new Vector2(m_offSet, 0));
	}
}
